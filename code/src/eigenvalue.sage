def _initialise_rings(prec=53):
    global PREC
    global RRING
    global CRING
    global TWO_PI
    global TWO_PI_I
    global CZERO
    global POLY_RING
    global IGUSA_GENS
    PREC = prec
    RRING = RealIntervalField(PREC)
    CRING = ComplexIntervalField(PREC)
    TWO_PI = RRING(2*pi)
    TWO_PI_I = TWO_PI*CRING(0, 1)
    CZERO = CRING(0, 0)
    POLY_RING = PolynomialRing(ZZ, 4, names=['a', 'b', 'c', 'd'])
    IGUSA_GENS = _init_gens()
    MAT_DICT = dict()
    return CRING, IGUSA_GENS


def eigenvalue_numerical(f, k, p, err, verbose=False, prec=None):
    if prec is None:
        prec = _estimate_prec(k, p, err)
        prec = prec + 3    # for luck
        estimated = True
    else:
        estimated = False
    CRING = _initialise_rings(prec)
    z = matrix([[CRING(0, 1), CZERO], [CZERO, CRING(0, 5)]])
    _clear_cache(f)

    ev, _, _ = _eigenvalue_T_with_error(f, k, z, p, err, verbose=verbose)

    if _diameter(ev) > err:
        if estimated:
            st = 'auto-estimated'
        else:
            st = 'given'
        raise RuntimeError("%s precision %s was insufficient to compute the eigenvalue %s to requested accuracy; please increase the precision via the optional argument 'prec='" % (st, prec, ev))
    return ev


def _eigenvalue_T_with_error(F, k, z, p, err, verbose=False):
    f, tilde_eps_f = _rough_estimate_F(F, k, z)
    fl = abs(f) - 2*tilde_eps_f
    fu = abs(f) + 2*tilde_eps_f

    # need the analogue here of
    # deligne = RDF(p)^((k-1)/2)
    # for non-lifts:
    deligne = 4*RDF(p)^(k-3/2)
    tpfu = deligne*fu
    tilde_eps_tpf = deligne*tilde_eps_f

    eps_tpf = min([err*fl/2, tilde_eps_tpf])
    eps_f = min([err*fl^2/(2*tpfu), tilde_eps_f])

    f, fT = _eval_F_with_error(F, k, z, eps_f)

    tpf, tpfT = _eval_TpF_with_error(F, k, z, p, eps_tpf)
    ev = tpf/f
    return ev, fT, tpfT


def _rough_estimate_F(F, k, z, mag=10):
    tilde_eps_F = 1/mag
    val, _ = _eval_F_with_error(F, k, z, tilde_eps_F)
    while (abs(val) - 2*tilde_eps_F) < 0:
        tilde_eps_F = tilde_eps_F/mag
        val, _ = _eval_F_with_error(F, z, tilde_eps_F)
    return val, tilde_eps_F


def _eval_TpF_with_error(F, k, z, p, err, verbose=False):
    s = CZERO
    err_F = err/(p^3+p^2+p+1)
    max_T = 0
    for (t, newZ) in _make_data(z, k, p):
        pass


def _eval_F_with_error(poly, z, err):
    a, b, c, d = poly.parent().gens()
    gen_at_z = [CZERO] * 4
    for j in range(4):
        if poly.degrees()[j] > 0:
            gen_at_z[j] = _eval_gen_with_error(IGUSA_GENS[j], z, err)

    return poly.subs({a: gen_at_z[0],
                      b: gen_at_z[1],
                      c: gen_at_z[2],
                      d: gen_at_z[3]})


def _eval_monomial_with_error(mon, z, err):
    pass


def _eval_gen_with_error(f, z, err):
    T = _find_T(f, z, err)
    lst = _indices_of_trace_up_to(T)
    return _eval_gen_at_list(f, z, lst)


def _eval_gen_at_list(f, z, lst=None):
    if lst is None:
        lst = f.coeffs()
    s = CZERO
    for T in lst:
        s = s + f[T]*(TWO_PI_I*((_qform2mat(T)*z).trace())).exp()
    return s


def _find_T(f, z, err):
    if f == IGUSA_GENS[0]:
        d = 5
        C = 19230
    elif f == IGUSA_GENS[1]:
        d = 9
        C = 12169
    elif f == IGUSA_GENS[2]:
        d = 11
        C = 220439
    elif f == IGUSA_GENS[3]:
        d = 13
        C = 287248

    alpha = RR(2*pi) * _broker_del(z)
    start = ((d+2)/alpha).unique_floor() + 1
    for T in range(start, 100000):
        test = RR(6*C*(d+3)/alpha*(-alpha*T).exp()*T*(d+2))
        if test < err:
            return T

    return 0


def _broker_del(z):
    y = _imag_mat(z)
    s = min(y[0, 0], y[1, 1])
    b = -y.trace()
    c = y.det()
    dd = b**2 - 4*c
    if dd < 0:
        return 2
    else:
        small_root = (-b-dd.sqrt())/2
        large_root = (-b+dd.sqrt())/2
        if (small_root < s < large_root):
            return small_root
        else:
            return s


def _imag_mat(z):
    lst = [a.imag() for a in z.list()]
    y = z.parent().change_ring(z.base_ring()._real_field())(lst)
    return y


def _real_mat(z):
    lst = [a.real() for a in z.list()]
    x = z.parent().change_ring(z.base_ring()._real_field())(lst)
    return x


def _qform2mat(T):
    (n, r, m) = T
    return Matrix(CRING, 2, 2, [n, r/2, r/2, m])


def _indices_of_trace_up_to(T):
    lst = []
    for t in range(T+1):
        lst = lst + _indices_of_trace(t)
    return lst


def _indices_of_trace(t):
    lst = []
    for a in range(t+1):
        c = t - a
        bmax = 2*RR(a*c).sqrt()
        for b in range(-bmax, bmax+1):
            lst.append((a, b, c))
    return lst


def _diameter(x):
    left, right = x.real().endpoints()
    return right - left


def _round(x):
    return x.endpoints()[0].round()


def _slash_operator(m, z):
    (a, b, c, d) = m
    return (a*z+b)*(c*z+d).inverse()


def _tp_cosets(p):
    rep = (m(p, 0, 0, p),
           m(0, 0, 0, 0),
           m(0, 0, 0, 0),
           m(1, 0, 0, 1))
    lst = [rep]

    for a in range(p):
        for b in range(p):
            for c in range(p):
                rep = (m(1, 0, 0, 1),
                       m(a, b, b, c),
                       m(0, 0, 0, 0),
                       m(p, 0, 0, p))
                lst.append(rep)

    for a in range(p):
        rep = (m(0, -p, 1, 0),
               m(0,  0, a, 0),
               m(0,  0, 0, 0),
               m(0, -1, p, 0))
        lst.append(rep)

    for a in range(p):
        for b in range(p):
            rep = (m(p, 0, -b, 1),
                   m(0, 0,  0, a),
                   m(0, 0,  0, 0),
                   m(1, b,  0, p))
            lst.append(rep)

    return lst


def m(a, b, c, d):
    if (a, b, c, d) not in MAT_DICT:
        MAT_DICT[(a, b, c, d)] = Matrix(CRING, 2, 2, [a, b, c, d])
    return MAT_DICT[(a, b, c, d)]


def _init_gens(SMF_PREC=100):
    from sage.modular.siegel.all import SiegelModularFormsAlgebra
    gens = SiegelModularFormsAlgebra(default_prec=SMF_PREC).gens()
    # return [f.coeffs() for f in gens]
    return gens
