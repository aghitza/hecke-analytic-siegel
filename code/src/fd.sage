def _sp4_reduce(Z):
    Zred = Z
    M = identity_matrix(ZZ, 4)
    lst = [0, 0, -1, 0,
           0, 1,  0, 0,
           1, 0,  0, 0,
           0, 0,  0, 1]
    N0 = matrix(ZZ, 4, 4, lst)
    I = identity_matrix(2)
    zero = zero_matrix(2)
    while not _is_sp4_reduced(Zred):
        # gl2-reduce the imaginary part
        Y = _imag_part(Zred)
        Yred, U = _gl2_reduce(Y)
        mat = block_diagonal_matrix(U, U.transpose().inverse())
        M = mat*M
        Zred = U*Zred*U.transpose()
        # reduce the real part
        X = _real_part(Zred)
        r1 = _real_reduce(X[0, 0])
        r3 = _real_reduce(X[0, 1])
        r2 = _real_reduce(X[1, 1])
        B = matrix(ZZ, 2, [r1, r3, r3, r2])
        mat = block_matrix(2, [I, B, zero, I])
        M = mat*M
        Zred = Zred + B
        # bring into region B
        d = _star(N0, Zred).det().abs()
        R = d.parent()
        if not _leq(R(1), d):
            M = N0*M
            Zred = _sp_action(N0, Zred)
    return (Zred, M)


def _sp_action(mat, Z):
    mat.subdivide(2, 2)
    A = mat.subdivision(0, 0)
    B = mat.subdivision(0, 1)
    C = mat.subdivision(1, 0)
    D = mat.subdivision(1, 1)
    res = (A*Z+B)*(C*Z+D).inverse()
    return res


def _is_sp4_reduced(Z):
    return _is_sp4_reduced_S1(Z) and _is_sp4_reduced_S2(Z) and _is_sp4_reduced_S3_B(Z)


def _is_sp4_reduced_S1(Z):
    X = _real_part(Z)
    x1 = X[0, 0]
    x3 = X[0, 1]
    x2 = X[1, 1]
    RRING = x1.parent()
    minushalf = RRING(-0.5)
    half = RRING(0.5)
    def _test(x):
        return _leq(minushalf, x) and _leq(x, half)
    return _test(x1) and _test(x2) and _test(x3)


def _is_sp4_reduced_S2(Z):
    Y = _imag_part(Z)
    return _is_gl2_reduced(Y)


def _is_sp4_reduced_S3_B(Z):
    lst = [0, 0, -1, 0,
           0, 1,  0, 0,
           1, 0,  0, 0,
           0, 0,  0, 1]
    N0 = matrix(ZZ, 4, 4, lst)
    d = _star(N0, Z).det().abs()
    R = d.parent()
    return _leq(R(1), d)


def _star(M, Z):
    M.subdivide(2, 2)
    C = M.subdivision(1, 0)
    D = M.subdivision(1, 1)
    return C*Z + D


def _real_part(Z):
    prec = Z.base_ring().precision()
    RRING = RealIntervalField(prec)
    return matrix(RRING, 2, 2, [z.real() for z in Z.list()])


def _imag_part(Z):
    prec = Z.base_ring().precision()
    RRING = RealIntervalField(prec)
    return matrix(RRING, 2, 2, [z.imag() for z in Z.list()])


def _gl2_reduce(Y):
    Yred, U = _sl2_reduce(Y)
    if not _is_gl2_reduced(Yred):
        mat = matrix(ZZ, 2, 2, [1, 0, 0, -1])
        U = mat*U
        Yred = mat*Yred*mat.transpose()
    return (Yred, U)


def _is_gl2_reduced(Y):
    y1 = Y[0, 0]
    y3 = Y[0, 1]
    y2 = Y[1, 1]
    RING = y1.parent()
    return _leq(RING(0), y3) and _leq(2*y3, y1) and _leq(y1, y2)


def _sl2_reduce(Y):
    Yred = Y
    U = identity_matrix(ZZ, 2)
    inv = matrix(ZZ, 2, 2, [0, 1, -1, 0])
    while not _is_sl2_reduced(Yred):
        y1 = Yred[0, 0]
        y3 = Yred[0, 1]
        y2 = Yred[1, 1]
        r = _real_reduce(y3/y1)
        mat = matrix(ZZ, 2, [1, 0, r, 1])
        U = mat*U
        Yred = mat*Yred*mat.transpose()
        y1 = Yred[0, 0]
        y2 = Yred[1, 1]
        if y1 > y2:
            U = inv*U
            Yred = inv*Yred*inv.transpose()
    return (Yred, U)


def _real_reduce(x):
    t = -x + 0.5
    try:
        r = t.unique_floor()
    except ValueError:
        r = t.unique_integer()
    return r


def _is_sl2_reduced(Y):
    y1 = Y[0, 0]
    y3 = Y[0, 1]
    y2 = Y[1, 1]
    return _leq(-y1, 2*y3) and _leq(2*y3, y1) and _leq(y1, y2)


def _leq(left, right):
    # this might be a bit too permissive
    return left.endpoints()[0] <= right.endpoints()[0]


def _Ytoz(Y):
    RRING = Y.base_ring()
    y1 = Y[0, 0]
    y3 = Y[0, 1]
    y2 = Y[1, 1]
    det = y1*y2 - y3^2
    assert det > 0
    prec = RRING.precision()
    CRING = ComplexIntervalField(prec)
    x = -y3/y1
    y = det.sqrt()/y1
    return CRING(x, y)


def _ztoY(z):
    CRING = z.parent()
    a = z.real()
    b = z.imag()
    assert b > 0
    prec = CRING.precision()
    RRING = RealIntervalField(prec)
    y1 = RRING(1)
    y2 = a^2 + b^2
    y3 = -a
    return matrix(RRING, 2, 2, [y1, y3, y3, y2])
