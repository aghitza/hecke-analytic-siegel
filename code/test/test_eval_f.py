import pytest
from sage.all_cmdline import matrix, i
from src.eigenvalue_sage import _eval_gen_with_error, _initialise_rings


class TestEvalGen(object):

    def test_eval_c(self):
        CRING, IGUSA_GENS = _initialise_rings()
        c = IGUSA_GENS[2]
        z = matrix(CRING, 2, 2, [5*i, i, i, 6*i])
        err = 10**(-2)
        res = _eval_gen_with_error(c, z, err)
        assert res > 1.28*10**(-28)

