import pytest
from sage.all_cmdline import identity_matrix, matrix, GF
from src.fd_sage import _is_symplectic


class TestUtils(object):

    def test_is_symplectic_identity(self):
        m = identity_matrix(4)
        assert _is_symplectic(m) is True

    # def test_is_symplectic_one_true(self):
    #     m = matrix(GF(3), [[0, 0, 0, 1], [0, 0, 1, 0], [0, 2, 0, 1], [2, 0, 1, 2]])
    #     assert _is_symplectic(m) is True

    # def test_is_symplectic_one_false(self):
    #     m = matrix(GF(2), [[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]])
    #     assert _is_symplectic(m) is False
