import pytest
from sage.all_cmdline import matrix, ComplexIntervalField, RealIntervalField
from src.fd_sage import _Ytoz, _ztoY, _is_sl2_reduced, _sl2_reduce, _is_gl2_reduced, _gl2_reduce


class TestUtils(object):

    def test_Ytoz_identity(self):
        RRING = RealIntervalField()
        CRING = ComplexIntervalField()
        Y = matrix(RRING, 2, 2, [1, 0, 0, 1])
        res = _Ytoz(Y)
        z = CRING(0, 1)
        assert res == z

    def test_ztoY_i(self):
        CRING = ComplexIntervalField()
        RRING = RealIntervalField()
        z = CRING(0, 1)
        res = _ztoY(z)
        Y = matrix(RRING, 2, 2, [1, 0, 0, 1])
        assert res == Y

    def test_Ytoz_ztoY_loop(self):
        RRING = RealIntervalField()
        CRING = ComplexIntervalField()
        for a in range(-2, 3):
            for b in range(1, 6):
                z = CRING(a, b)
                Y = _ztoY(z)
                z2 = _Ytoz(Y)
                assert z == z2


class TestIsSL2Red(object):

    def test_is_sl2_reduced_i(self):
        CRING = ComplexIntervalField()
        z = CRING(0, 1)
        Y = _ztoY(z)
        assert _is_sl2_reduced(Y) is True

    def test_is_sl2_reduced_half_i(self):
        CRING = ComplexIntervalField()
        z = CRING(0, 0.5)
        Y = _ztoY(z)
        assert _is_sl2_reduced(Y) is False

    def test_is_sl2_reduced_another(self):
        CRING = ComplexIntervalField()
        z = CRING(3, 1)
        Y = _ztoY(z)
        assert _is_sl2_reduced(Y) is False


class TestSL2Red(object):

    def test_sl2_reduce_i(self):
        CRING = ComplexIntervalField()
        z = CRING(0, 1)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        zred = _Ytoz(Yred)
        assert z == zred

    def test_sl2_reduce_inversion(self):
        CRING = ComplexIntervalField()
        z = CRING(0.5, 0.5)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        res = _Ytoz(Yred)
        zred = CRING(0, 1)
        assert res == zred

    def test_sl2_reduce_translate_left(self):
        CRING = ComplexIntervalField()
        z = CRING(3.1, 2)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        res = _Ytoz(Yred)
        zred = CRING(0.1, 2)
        err = (res - zred).abs()
        assert err < 10**(-10)

    def test_sl2_reduce_translate_right(self):
        CRING = ComplexIntervalField()
        z = CRING(-3.1, 2)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        res = _Ytoz(Yred)
        zred = CRING(-0.1, 2)
        err = (res - zred).abs()
        assert err < 10**(-10)

    def test_sl2_reduce_mixed(self):
        CRING = ComplexIntervalField()
        z = CRING(0.4, 0.1)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        res = _Ytoz(Yred)
        zred = CRING(-0.25, 1.25)
        err = (res - zred).abs()
        assert err < 10**(-10)

    def test_sl2_reduce_another(self):
        CRING = ComplexIntervalField()
        RRING = RealIntervalField()
        x = RRING(3)**(-1)
        z = CRING(x, x)
        Y = _ztoY(z)
        Yred, _ = _sl2_reduce(Y)
        res = _Ytoz(Yred)
        zred = CRING(-0.5, 1.5)
        err = (res - zred).abs()
        assert err < 10**(-10)


class TestIsGL2Red(object):

    def test_is_gl2_reduced_i(self):
        CRING = ComplexIntervalField()
        z = CRING(0, 1)
        Y = _ztoY(z)
        assert _is_gl2_reduced(Y) is True

    def test_sl2_reduced_not_gl2_reduced(self):
        RRING = RealIntervalField()
        Y = matrix(RRING, 2, 2, [2, -1, -1, 3])
        assert _is_sl2_reduced(Y) is True
        assert _is_gl2_reduced(Y) is False


class TestGL2Red(object):

    def test_gl2_reduce(self):
        RRING = RealIntervalField()
        Y = matrix(RRING, 2, 2, [2, -1, -1, 3])
        res, _ = _gl2_reduce(Y)
        Yred = matrix(RRING, 2, 2, [2, 1, 1, 3])
        assert res == Yred
