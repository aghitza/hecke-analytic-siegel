import pytest
from sage.all_cmdline import matrix, ComplexIntervalField, RealIntervalField, i
from src.fd_sage import _is_sp4_reduced, _sp4_reduce


class TestIsSp4Red(object):

    def test_is_sp4_reduced(self):
        CRING = ComplexIntervalField()
        Z = matrix(CRING, 2, 2, [5*i, i, i, 6*i])
        assert _is_sp4_reduced(Z) is True


class TestSp4Red(object):

    def test_sp4_reduce(self):
        CRING = ComplexIntervalField()
        lst = [2+5*i, 13+26*i, 13+26*i, 83+141*i]
        Z = matrix(CRING, 2, 2, lst)
        res, _ = _sp4_reduce(Z)
        lst = [5*i, i, i, 6*i]
        Zred = matrix(CRING, 2, 2, lst)
        assert res == Zred

    def test_sp4_reduce(self):
        CRING = ComplexIntervalField()
        lst = [i*3**(-1), 0, 0, i*3**(-1)]
        Z = matrix(CRING, 2, 2, lst)
        res, _ = _sp4_reduce(Z)
        lst = [3*i, 0, 0, 3*i]
        Zred = matrix(CRING, 2, 2, lst)
        assert res == Zred

