import pytest
from sage.all_cmdline import matrix, CC, RR, i
from src.eigenvalue_sage import _indices_of_trace, _indices_of_trace_up_to, _imag_mat, _real_mat


class TestUtils(object):

    def test_indices_of_trace_zero(self):
        res = _indices_of_trace(0)
        assert res == [(0, 0, 0)]

    def test_indices_of_trace_one(self):
        res = _indices_of_trace(1)
        assert res == [(0, 0, 1), (1, 0, 0)]

    def test_indices_of_trace_two(self):
        res = _indices_of_trace(2)
        assert res == [(0, 0, 2), (1, -2, 1), (1, -1, 1), (1, 0, 1), (1, 1, 1), (1, 2, 1), (2, 0, 0)]

    def test_indices_of_trace_three(self):
        res = _indices_of_trace(3)
        assert len(res) == 12

    def test_indices_of_trace_up_to_three(self):
        res = _indices_of_trace_up_to(3)
        assert len(res) == 22

    def test_real_mat(self):
        z = matrix(CC, 2, 2, [1+i, 1-i, 2, 3*i])
        res = _real_mat(z)
        y = matrix(RR, 2, 2, [1, 1, 2, 0])
        assert res == y

    def test_imag_mat(self):
        z = matrix(CC, 2, 2, [1+i, 1-i, 2, 3*i])
        res = _imag_mat(z)
        y = matrix(RR, 2, 2, [1, -1, 0, 3])
        assert res == y

    # def test_is_symplectic_one_true(self):
    #     m = matrix(GF(3), [[0, 0, 0, 1], [0, 0, 1, 0], [0, 2, 0, 1], [2, 0, 1, 2]])
    #     assert _is_symplectic(m) is True

    # def test_is_symplectic_one_false(self):
    #     m = matrix(GF(2), [[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]])
    #     assert _is_symplectic(m) is False
